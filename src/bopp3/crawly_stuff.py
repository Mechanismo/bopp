#!/usr/bin/python
import os
import sys
script_path = "/home/pi/crawly-cone-bot/python"
sys.path.append(script_path)

import rospy
import time
import math

from time import sleep

import crawly_tcp

# list of ip addresses to try
IP_ADDRESSES = [
    "192.168.0.191",    # roam
    "192.168.2.235",    # work
    "192.168.2.234",    # work
    #"192.168.123.67",   # home
    "192.168.4.1",      # AP mode
]
PORTNUM = 8880

DXL_ID = [1,2,3,4]
DXL_RANGE = 3698
DXL_SAFETY = 100

target_servo_idx = 0

def connect():
    global IP_ADDRESSES
    global PORTNUM

    global is_connecting
    if is_connecting: return False
    is_connecting = True

    rospy.loginfo("TCP connect()")
    rospy.loginfo("{} address".format(len(IP_ADDRESSES)))
    for i in range(0,5):
        rospy.loginfo("LOOP {}".format(i))
        for ip in IP_ADDRESSES:
            rospy.loginfo("IP {}".format(ip))
            rospy.loginfo("Trying {}:{}...".format(ip,PORTNUM))
            if crawly_tcp.connect(ip,PORTNUM):
                break
        if not crawly_tcp.IPADDR:
            break

    if not crawly_tcp.IPADDR:
        rospy.loginfo("FAILED")
        is_connecting = False
        return False,None

    rospy.loginfo("CONNECTED")
    is_connecting = False
    return True,crawly_tcp.IPADDR

def close():
    crawly_tcp.close()

def reset():
    crawly_tcp.close()
    sleep(1)
    connect()

KEY_PATTERN = None
PATTERN_TORQUE_RELAX = [[1,64,0],[2,64,0],[3,64,0],[4,64,0]]
PATTERN_TORQUE_ENABLE = [[1,64,1],[2,64,1],[3,64,1],[4,64,1]]

def load_patterns():
    global KEY_PATTERN

    MIN=0
    MID=1
    MAX=2
    
    PATTERN_UP = [[1,116,dynamixel_range[1][MIN]],[2,116,dynamixel_range[2][MIN]],[3,116,dynamixel_range[3][MIN]],[4,116,dynamixel_range[4][MIN]]]
    PATTERN_DOWN = [[1,116,dynamixel_range[1][MID]],[2,116,dynamixel_range[2][MID]],[3,116,dynamixel_range[3][MID]],[4,116,dynamixel_range[4][MID]]]
    PATTERN_RIGHT_REAR_UP = [[1,116,dynamixel_range[1][MID]],[2,116,dynamixel_range[2][MIN]],[3,116,dynamixel_range[3][MIN]],[4,116,dynamixel_range[4][MIN]]]
    PATTERN_LEFT_REAR_UP = [[1,116,dynamixel_range[1][MIN]],[2,116,dynamixel_range[2][MID]],[3,116,dynamixel_range[3][MIN]],[4,116,dynamixel_range[4][MIN]]]
    PATTERN_RIGHT_FRONT_UP = [[1,116,dynamixel_range[1][MIN]],[2,116,dynamixel_range[2][MIN]],[3,116,dynamixel_range[3][MID]],[4,116,dynamixel_range[4][MIN]]]
    PATTERN_LEFT_FRONT_UP = [[1,116,dynamixel_range[1][MIN]],[2,116,dynamixel_range[2][MIN]],[3,116,dynamixel_range[3][MIN]],[4,116,dynamixel_range[4][MID]]]
    PATTERN_LEAN_FORWARD = [[1,116,dynamixel_range[1][MIN]],[2,116,dynamixel_range[2][MIN]],[3,116,dynamixel_range[3][MID]],[4,116,dynamixel_range[4][MID]]]
    PATTERN_LEAN_BACK = [[1,116,dynamixel_range[1][MID]],[2,116,dynamixel_range[2][MID]],[3,116,dynamixel_range[3][MIN]],[4,116,dynamixel_range[4][MIN]]]
    PATTERN_LEAN_LEFT = [[1,116,dynamixel_range[1][MIN]],[2,116,dynamixel_range[2][MID]],[3,116,dynamixel_range[3][MIN]],[4,116,dynamixel_range[4][MID]]]
    PATTERN_LEAN_RIGHT = [[1,116,dynamixel_range[1][MID]],[2,116,dynamixel_range[2][MIN]],[3,116,dynamixel_range[3][MID]],[4,116,dynamixel_range[4][MIN]]]
    PATTERN_BALANCE_RR_LF = [[1,116,dynamixel_range[1][MID]],[2,116,dynamixel_range[2][MIN]],[3,116,dynamixel_range[3][MIN]],[4,116,dynamixel_range[4][MID]]]
    PATTERN_BALANCE_LR_RF = [[1,116,dynamixel_range[1][MIN]],[2,116,dynamixel_range[2][MID]],[3,116,dynamixel_range[3][MID]],[4,116,dynamixel_range[4][MIN]]]
    
    PATTERN_STOP = [[256,1,0],[257,1,0],[258,1,0],[259,1,0]]
    #PATTERN_FORWARD = [[256,1,DRIVE],[256,2,0],[257,1,DRIVE],[257,2,0],[258,1,DRIVE],[258,2,0],[259,1,DRIVE],[259,2,0]]
    #PATTERN_REVERSE = [[256,1,DRIVE],[256,2,1],[257,1,DRIVE],[257,2,1],[258,1,DRIVE],[258,2,1],[259,1,DRIVE],[259,2,1]]
    #PATTERN_SPIN_LEFT = [[256,1,motor_fast_pwm],[256,2,0],[257,1,motor_fast_pwm],[257,2,1],[258,1,motor_fast_pwm],[258,2,0],[259,1,motor_fast_pwm],[259,2,1]]
    #PATTERN_SPIN_RIGHT = [[256,1,motor_fast_pwm],[256,2,1],[257,1,motor_fast_pwm],[257,2,0],[258,1,motor_fast_pwm],[258,2,1],[259,1,motor_fast_pwm],[259,2,0]]

    KEY_PATTERN = {
        '0': PATTERN_UP,
        '1': PATTERN_LEFT_REAR_UP,
        '2': PATTERN_LEAN_BACK,
        '3': PATTERN_RIGHT_REAR_UP,
        '4': PATTERN_LEAN_LEFT,
        '5': PATTERN_DOWN,
        '6': PATTERN_LEAN_RIGHT,
        '7': PATTERN_LEFT_FRONT_UP,
        '8': PATTERN_LEAN_FORWARD,
        '9': PATTERN_RIGHT_FRONT_UP,

        'F': PATTERN_UP,
        'Z': PATTERN_LEFT_REAR_UP,
        'X': PATTERN_LEAN_BACK,
        'C': PATTERN_RIGHT_REAR_UP,
        'A': PATTERN_LEAN_LEFT,
        'S': PATTERN_DOWN,
        'D': PATTERN_LEAN_RIGHT,
        'Q': PATTERN_LEFT_FRONT_UP,
        'W': PATTERN_LEAN_FORWARD,
        'E': PATTERN_RIGHT_FRONT_UP,


        '/': PATTERN_BALANCE_LR_RF,
        '\\': PATTERN_BALANCE_RR_LF,
        '*': PATTERN_BALANCE_RR_LF,
        '`': PATTERN_BALANCE_RR_LF,
        ' ': PATTERN_STOP,
    }

torque_enabled = False
def torque_enable(state):
    global torque_enabled
    if state:
        pat = PATTERN_TORQUE_ENABLE
    else:
        pat = PATTERN_TORQUE_RELAX

    crawly_tcp.send_pattern(pat)
    torque_enabled = state

def toggle_torque_enable():
    global torque_enabled
    torque_enable(not torque_enabled)

def get_torque_state():
    global torque_enabled
    return torque_enabled

def init():
    global p, q, a, b
    global wheel_distance
    global wheel_radius
    global MULTIPLIER_STANDARD
    global MULTIPLIER_PIVOT
    global BASE_PWM
    global MAX_PWM

    wheel_distance = 0.27   # ...ish! :)
    wheel_radius = 0.108
    MULTIPLIER_STANDARD = 0.15
    MULTIPLIER_PIVOT = 3.0
    BASE_PWM = 120
    MAX_PWM = 240

    if get_connected():
        if not dynamixels_zeroed:
            if not zero_dynamixels(): return

is_connecting = False

def get_connected():
    try:
        if crawly_tcp.IPADDR:
            #rospy.loginfo("already connected")

            if not dynamixels_zeroed:
                zero_dynamixels()

            return True
        else:
            status, ip_address = connect()
            if status:
                rospy.loginfo("connected to {}".format(ip_address))

                if not dynamixels_zeroed:
                    zero_dynamixels()
            else:
                rospy.loginfo("FAILED")
            return status
    except:
        return False
    return False

def forward(speed):
    if not get_connected(): return False
    try:
        crawly_tcp.write_register(288,108,[speed,0,0,0,0,0])
    except:
        # will try to reopen on next command
        rospy.loginfo("CRAWLY TCP FAILED")
        crawly_tcp.close()

    # TODO: needed for debugging?
    # ret = crawly_tcp.read_register(288,108)

def reverse(speed):
    if not get_connected(): return False
    try:
        crawly_tcp.write_register(288,108,[-speed,0,0,0,0,0])
    except:
        # will try to reopen on next command
        rospy.loginfo("CRAWLY TCP FAILED")
        crawly_tcp.close()

def key_pattern(ch):
    if ch in KEY_PATTERN:
        pattern = KEY_PATTERN[ch]
        crawly_tcp.send_pattern(pattern)
        return True
    return False

def set_cmd_vel(linear_speed, angular_speed):
    """
        rospy.loginfo("linear: {} angular: {}".format(linear_speed,angular_speed))
        
        if not get_connected(): 
            rospy.loginfo("NOT CONNECTED")
            return False

        crawly_tcp.write_register(288,108,[linear_speed,0,0,0,0,angular_speed])
        crawly_tcp.write_register(288,110,500)
    """
    if not get_connected(): return False
    # TODO Make use of this ? ...
    # body turn radius...
    if angular_speed != 0.0:
        body_turn_radius = linear_speed / angular_speed
        right_wheel_turn_radius = body_turn_radius + ( 1 * (wheel_distance / 2.0))
        left_wheel_turn_radius = body_turn_radius + ( -1 * (wheel_distance / 2.0))

        left_rpm = (angular_speed * left_wheel_turn_radius) / wheel_radius
        right_rpm = (angular_speed * right_wheel_turn_radius) / wheel_radius

    else:
        # Not turning, infinite turn radius
        left_rpm = linear_speed / wheel_radius
        right_rpm = left_rpm

    if math.copysign(1,right_rpm) != math.copysign(1,left_rpm):
        multiplier = MULTIPLIER_PIVOT
    else:
        multiplier = MULTIPLIER_STANDARD
    
    left_pwm = int(left_rpm * multiplier * BASE_PWM)
    left_dir = 0
    if left_pwm < 0:
        left_pwm = abs(left_pwm)
        left_dir = 1

    right_pwm = int(right_rpm * multiplier * BASE_PWM)
    right_dir = 0
    if right_pwm < 0:
        right_pwm = abs(right_pwm)
        right_dir = 1
    
    regvals = [[256,1,right_pwm],[256,2,right_dir],[257,1,left_pwm],[257,2,left_dir],[258,1,right_pwm],[258,2,right_dir],[259,1,left_pwm],[259,2,left_dir]]
    try:
        crawly_tcp.send_pattern(regvals)
    except:
        # will try to reopen on next command
        rospy.loginfo("CRAWLY TCP FAILED")
        crawly_tcp.close()

dynamixels_zeroed = False
dynamixel_range = {}
def zero_dynamixels():
    global dynamixels_zeroed
    global dynamixel_range
    for id in DXL_ID:
        dxl_min = crawly_tcp.read_position(id)
        if dxl_min is None: return False

        # (min/mid/max) as a tuple
        dynamixel_range[id] = (dxl_min,dxl_min+2000,dxl_min + DXL_RANGE - DXL_RANGE)
    dynamixels_zeroed = True
    load_patterns()
    return True

def read_position(id):
    return crawly_tcp.read_position(id)

def test_dynamixel():
    tstart = time.time()
    while True:
        test = crawly_tcp.read_register(1,7)
        if test is None:
            print("Error reading dynamixel ID:1")
        else:
            break
        if time.time()-tstart > 10:
            print("TIMEOUT reading from robot.")
        sleep(0.2)

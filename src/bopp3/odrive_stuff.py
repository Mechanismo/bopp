#!/usr/bin/python

import rospy
import time
import odrive
import math
from odrive.enums import *

odrv0 = None

def init():
    global p, q, a, b
    global wheel_distance
    global wheel_radius
    global MULTIPLIER_STANDARD
    global MULTIPLIER_PIVOT
    global BASE_PWM
    global MAX_PWM

    wheel_distance = 0.42
    wheel_radius = 0.28
    MULTIPLIER_STANDARD = 1.5
    MULTIPLIER_PIVOT = 6.0
    BASE_PWM = 50
    MAX_PWM = 100


def get_odrv0():
    global odrv0

    try:
        if odrv0 is None:
            odrv0 = odrive.find_any(timeout=2)

        return odrv0
    except:
        return None

def reset_odrive():
    global odrv0

    try:
        odrv0 = odrive.find_any(timeout=10)
        if odrv0 is None:
            return False

            if not clear_errors():
                return False

            calibrate()

            if not clear_errors():
                return False

            # odrv0.axis1.controller.config.control_mode = CONTROL_MODE_POSITION_CONTROL
            odrv0.axis0.controller.input_vel = 0
            odrv0.axis1.controller.input_vel = 0
            odrv0.axis0.controller.config.control_mode = CONTROL_MODE_VELOCITY_CONTROL
            odrv0.axis1.controller.config.control_mode = CONTROL_MODE_VELOCITY_CONTROL

            odrv0.axis0.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL
            odrv0.axis1.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL

            return True
    except:
        return False
    # print(str(odrv0.vbus_voltage))

def clear_axis_errors(odaxis):
    global odrv0

    if odaxis.error != 0:
        print("clearing axis error...")
        odaxis.error = 0
        if odaxis.error != 0:
            print("failed!")
            return False
    if odaxis.motor.error != 0:
        print("clearing motor error...")
        odaxis.motor.error = 0
        if odaxis.motor.error != 0:
            print("failed!")
            return False
    if odaxis.encoder.error != 0:
        print("clearing encoder error...")
        odaxis.encoder.error = 0
        if odaxis.encoder.error != 0:
            print("failed!")
            return False
    if odaxis.controller.error != 0:
        print("clearing controller error...")
        odaxis.controller.error = 0
        if odaxis.controller.error != 0:
            print("failed!")
            return False
    return True            

def clear_errors():
    global odrv0
    if odrv0 is None: return False
    ret0 = clear_axis_errors(odrv0.axis0)
    ret1 = clear_axis_errors(odrv0.axis1)
    return ret0 and ret1


def calibrate():
    print("calibrating[0]...")
    odrv0.axis0.requested_state = AXIS_STATE_FULL_CALIBRATION_SEQUENCE

    # wait for calibration
    while odrv0.axis0.current_state != AXIS_STATE_IDLE and odrv0.axis1.error == 0: 
        time.sleep(0.1)

    print("calibrating[1]...")
    odrv0.axis1.requested_state = AXIS_STATE_FULL_CALIBRATION_SEQUENCE

    # wait for calibration
    while odrv0.axis1.current_state != AXIS_STATE_IDLE and odrv0.axis1.error == 0: 
        time.sleep(0.1)

    print("done")

def forward(speed):
    odrv = get_odrv0()
    if odrv is None: return False

    if odrv.axis0.controller.input_vel < 0:
        odrv.axis0.controller.input_vel = 0
        time.sleep(0.2)
    if odrv.axis1.controller.input_vel > 0:
        odrv.axis1.controller.input_vel = 0
        time.sleep(0.2)

    odrv.axis0.controller.input_vel = speed
    odrv.axis1.controller.input_vel = -speed

def reverse(speed):
    odrv = get_odrv0()
    if odrv is None: return False

    if odrv.axis0.controller.input_vel > 0:
        odrv.axis0.controller.input_vel = 0
        time.sleep(0.2)
    if odrv.axis1.controller.input_vel < 0:
        odrv.axis1.controller.input_vel = 0
        time.sleep(0.2)

    odrv.axis0.controller.input_vel = -speed
    odrv.axis1.controller.input_vel = speed

def set_cmd_vel(linear_speed, angular_speed):
    odrv = get_odrv0()
    if odrv is None: return False
    #odrv.axis0.controller.input_vel = linear_speed * 4
    #odrv.axis1.controller.input_vel = -linear_speed * 4

    # body turn radius...
    if angular_speed != 0.0:
        body_turn_radius = linear_speed / angular_speed
        right_wheel_turn_radius = body_turn_radius + ( 1 * (wheel_distance / 2.0))
        left_wheel_turn_radius = body_turn_radius + ( -1 * (wheel_distance / 2.0))

        left_rpm = (angular_speed * left_wheel_turn_radius) / wheel_radius
        right_rpm = (angular_speed * right_wheel_turn_radius) / wheel_radius

    else:
        # Not turning, infinite turn radius
        left_rpm = linear_speed / wheel_radius
        right_rpm = left_rpm

    if math.copysign(1,right_rpm) != math.copysign(1,left_rpm):
        multiplier = MULTIPLIER_PIVOT
    else:
        multiplier = MULTIPLIER_STANDARD

    #left_pwm = int(left_rpm * multiplier * BASE_PWM)
    #right_pwm = int(right_rpm * multiplier * BASE_PWM)
    
    # motor_pwm(left_pwm,right_pwm)
    try:
        odrv.axis0.controller.input_vel = right_rpm * multiplier 
        odrv.axis1.controller.input_vel = -left_rpm * multiplier 
    except:
        # rospy.loginfo("VEL L,R %f, %f"%(odrv.axis1.controller.input_vel,odrv.axis0.controller.input_vel))
        rospy.loginfo("ODRIVE ERROR")
        reset_odrive()


# reset_odrive()    



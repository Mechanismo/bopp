#!/bin/sh
#export ROS_HOSTNAME=$(hostname).local
#export ROS_HOSTNAME=$(hostname).hexray.x
#export ROS_HOSTNAME=192.168.2.33

ip4=$(/sbin/ip -o -4 addr list wlan0 | awk '{print $4}' | cut -d/ -f1)

export ROS_HOSTNAME=$ip4
export ROS_MASTER_URI=http://$ROS_HOSTNAME:11311
echo $ROS_MASTER_URI

# To install services...

## /etc/systemd/system/roscore.service

Starts roscore, after verifying network is up.

```sh
sudo ln -s /home/pi/catkin_ws/src/bopp/services/roscore.service /etc/systemd/system/roscore.service
```
## /etc/ros/env.sh

```sh
sudo ln -s /home/pi/catkin_ws/src/bopp/services/env.sh /etc/ros/env.sh
```

## /etc/systemd/system/roslaunch.service

```sh
sudo ln -s /home/pi/catkin_ws/src/bopp/services/roslaunch.service /etc/systemd/system/roslaunch.service
```

## /usr/sbin/roslaunch


```sh
sudo ln -s /home/pi/catkin_ws/src/bopp/services/roslaunch /usr/sbin/roslaunch
sudo chmod +x /usr/sbin/roslaunch
```
# bopp

Everyone should have a terrible robot.  Bopp the robot is mine.

Despite initial good intentions of laser cut and 3D printed parts, Bopp has degenerated into a tangle of boards and periphersals, envined with wires, all cemented in place with gobs of hot glue.  But I have learned an awful lot tinkering on Bopp and despite his scruffy exterior, he is becoming a rather sophistacted little robot.  I have kind of grown rather fond of bopp.

The time has come to perform a few serious upgrades, so I use this opportunity to document a few things.  At the worst at least I will have some reference when I get stuck putting him back together.

## Specifications

- sbc : Raspberry Pi 3B
- memory : 64GB SD-flash
- motors : 4 x micro metal gear motor 50:1
- motor driver : Robohat
- batteries : 2 x 2000mAh 2S lipos paralleled (both deceased)
- power : 5V DC:DC converter for lidar
- charging : tail is connected to bat +ve and 2x bumpers wired to ground
- I2C ADC
- IMU
- Neopixel 8 led array
- lidar
- sonar
- camera
- head servo : S3003
- tail servo : S3003
- remote control : Playstation dual shock 4 bluetooth game controller

left analog joystick : up/down, drives forwards and reverse
right analog joystick : steers left and right
left cursor pad : left, up, right - points head servo in these directions
touch pad : drag left and right steers head servo
analog trigger buttons : various tail waggling experiments
square button : toggle tail wag up / down.  useful for charging station
X button : toggle normal drive / drive ODrives / disable drive
O button : in ODrive mode, reset and calibrate ODrives
triangle button : select various neopixel patterns


## Software

Bopp is running a full ROS1 stack on Raspbien, all compiled from source.
This was to allow trying various SLAM and other ROS packakges that required up to date dependencies.

On startup bopp starts a service which starts a roscore instance.  A second service starts which loads various ROS services.

One serive waits for a game controller to connect on bluetooth, then interprets inputs from the controller to invoke various actions.









#!/usr/bin/env python

from __future__ import print_function

from beginner_tutorials.srv import ServoUpdate,ServoUpdateResponse,SpeedUpdate,SpeedUpdateResponse,PixelUpdate,PixelUpdateResponse

import rospy
from beginner_tutorials import robohat
from beginner_tutorials import odrive_stuff
from beginner_tutorials import crawly_stuff
import time
import math
import tf.transformations
from geometry_msgs.msg import Twist
from std_msgs.msg import Int16
from sensor_msgs.msg import Range, JointState

wag_freq = 0
wag_last = 0
cmdvel_mode = 0

odrive_calibrated = False

target_vel = None
vel_last_ns = 0
cmdvel_mode_publisher = None
sonar_publisher = None
joint_publisher = None
do_sonar = True
sonar_last_ns = 0

range_msg = Range()
range_msg.header.frame_id = "head_link"
range_msg.min_range = 0.02
range_msg.max_range = 2.0
range_msg.field_of_view = 0.5

joint_msg = JointState()
joint_msg.name.append("base_to_head")
joint_msg.position.append(0.0)
joint_msg.velocity.append(0.0)

range_msg.header.frame_id = "head_link"
range_msg.min_range = 0.02
range_msg.max_range = 2.0
range_msg.field_of_view = 0.5

head_link_angle = 0

def update_servo(req):
    global head_link_angle
    
    print("Set servo [%s = %s]"%(req.id, req.angle))
    robohat.setServo(req.id, req.angle)

    if req.id==0:
        head_link_angle = -req.angle / 180.0 * math.pi

    return ServoUpdateResponse(1)

def srv_forward(req):
    print("forward [%s]"%(req.speed))
    if cmdvel_mode == 0:
        robohat.forward(req.speed)
    elif cmdvel_mode == 1:
        # crab mode
        odrive_stuff.forward(req.speed)
    elif cmdvel_mode == 2:
        # crawly mode
        crawly_stuff.forward(req.speed)

    return SpeedUpdateResponse(1)

def srv_reverse(req):
    print("reverse [%s]"%(req.speed))
    if cmdvel_mode == 0:
        robohat.reverse(req.speed)
    elif cmdvel_mode == 1:
        # crab mode
        odrive_stuff.reverse(req.speed)
    elif cmdvel_mode == 2:
        # crawly mode
        crawly_stuff.reverse(req.speed)
    return SpeedUpdateResponse(1)

def srv_spinLeft(req):
    print("spin left [%s]"%(req.speed))
    robohat.spinLeft(req.speed)
    #robohat.turnForward(req.speed*0.3,req.speed)
    return SpeedUpdateResponse(1)

def srv_spinRight(req):
    print("spin right [%s]"%(req.speed))
    robohat.spinRight(req.speed)
    # robohat.turnForward(req.speed,req.speed*0.3)
    return SpeedUpdateResponse(1)

def srv_stop(req):
    print("stop")
    robohat.stop()
    return SpeedUpdateResponse(1)

def srv_wag(req):
    global wag_freq
    wag_freq = req.speed
    print("wag [%s]"%(req.speed))
    return SpeedUpdateResponse(1)

def reset_robot():
    # use to reset connected periferals like odrives / microcontroller etc
    if cmdvel_mode == 1:
        # odrive reset
        odrive_stuff.reset_odrive()
    elif cmdvel_mode == 2:
        # crawly tcp reset
        crawly_stuff.reset()

def srv_cmdvel_mode(req):
    global cmdvel_mode

    if req.id < 0:
        # special commands
        if req.r == 1:
            rospy.loginfo("RESET robot...")
            reset_robot()
    else:
        cmdvel_mode = req.id
        print("cmdvel mode [%s]"%(req.id))

        if cmdvel_mode == 1:
            # odrive mode
            # odrv0 = odrive.find_any()
            rospy.loginfo("connecting to odrive...")
            odrive_stuff.get_odrv0()
            rospy.loginfo("here2")
        elif cmdvel_mode == 2:
            # crawly mode
            rospy.loginfo("connecting to crawly...")
            crawly_stuff.get_connected()
        #cmdvel_mode = 1
        cmdvel_mode_publisher.publish(Int16(cmdvel_mode))

    return PixelUpdateResponse(1)

def cmd_vel_callback(msg):
    global target_vel
    target_vel = msg
    #print("cmdvel [%f, %f]"%(msg.linear.x, msg.angular.z))
    #robohat.set_cmd_vel(msg.linear.x, msg.angular.z)

def servo_server():
    global wag_freq
    global wag_last
    global target_vel
    global vel_last_ns
    global cmdvel_mode_publisher
    global sonar_publisher
    global range_msg
    global do_sonar
    global sonar_last_ns
    global joint_publisher
    global joint_msg
    global head_link_angle
    # global odrv0

    robohat.init()
    odrive_stuff.init()
    crawly_stuff.init()

    rospy.init_node('servo_server')

    rospy.Subscriber('cmd_vel', Twist, cmd_vel_callback)

    s = rospy.Service('servo', ServoUpdate, update_servo)
    s = rospy.Service('forward', SpeedUpdate, srv_forward)
    s = rospy.Service('reverse', SpeedUpdate, srv_reverse)
    s = rospy.Service('spinleft', SpeedUpdate, srv_spinLeft)
    s = rospy.Service('spinright', SpeedUpdate, srv_spinRight)
    s = rospy.Service('stop', SpeedUpdate, srv_stop)
    s = rospy.Service('wag', SpeedUpdate, srv_wag)
    s = rospy.Service('cmdvel_mode', PixelUpdate, srv_cmdvel_mode)

    cmdvel_mode_publisher = rospy.Publisher('cmdvel_mode_out', Int16, queue_size=1)

    sonar_publisher = rospy.Publisher('sonar', Range, queue_size=1)

    joint_publisher = rospy.Publisher("joint_states", JointState, queue_size=1)

    print("Ready to command servo.")
    #rospy.spin()
    #"""
    while not rospy.is_shutdown():
        tmns = time.time_ns()

        if not target_vel == None:
            #print(tmns - wag_last)
            # update 10Hz
            if tmns - vel_last_ns > 1e9 / 10:
                vel_last_ns = tmns
                if cmdvel_mode == 0:
                    robohat.set_cmd_vel(target_vel.linear.x, target_vel.angular.z)
                elif cmdvel_mode == 1:
                    # crab mode
                    odrive_stuff.set_cmd_vel(target_vel.linear.x, target_vel.angular.z)
                elif cmdvel_mode == 2:
                    # crawly mode
                    crawly_stuff.set_cmd_vel(target_vel.linear.x, target_vel.angular.z)

        if do_sonar:
            #print(tmns - wag_last)
            # update 10Hz
            if tmns - sonar_last_ns > 1e9 / 10:
                sonar_last_ns = tmns
                range_msg.header.stamp = rospy.Time.now()
                range_msg.range = robohat.getDistance() / 100.0
                sonar_publisher.publish(range_msg)

                joint_msg.position[0] = head_link_angle
                joint_msg.header.stamp = range_msg.header.stamp
                joint_publisher.publish(joint_msg)

        if wag_freq > 0:
            #print(tmns - wag_last)
            # update 50Hz
            if tmns - wag_last > 1e9 / 50:
                #print(".")
                wag_last = tmns
                tm =  float(tmns) / 1e9 * wag_freq * (2*math.pi)
                bval = 5 + 30 * math.sin(tm)
                robohat.setServo(1, bval)
    #"""

if __name__ == "__main__":
    servo_server()
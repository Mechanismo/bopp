#!/usr/bin/env python

import rospy
import math
import numpy
# from beginner_tutorials.Adafruit_ADS1x15.ADS1x15 import ADS1115
import board
import busio
i2c = busio.I2C(board.SCL, board.SDA)

import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

from std_msgs.msg import Float32MultiArray

if __name__ == '__main__':
    try:
        pub = rospy.Publisher('ADC', Float32MultiArray, queue_size=1)

        adc = ADS.ADS1115(i2c)

        values = [0,0,0,0]
        # TODO calibrate these...
        scale = [1,1,4.077825,4.077825]
        # values = Float32MultiArray()

        rospy.init_node('ADC')

        rate = rospy.Rate(10) # 10hz

        while not rospy.is_shutdown():
            try:
                for i in range(4):
                    chan = AnalogIn(adc, i)
                    values[i] = chan.voltage * scale[i]
                    # values.data.push_back(chan.voltage)

                array = Float32MultiArray(data=values)
                pub.publish(array)
            except OSError:
                # Getting occasional I2C errors on Pi
                # timing errors, or noise?
                warnmsg = "OSError reading I2C ADC"
                rospy.logwarn(warnmsg)
                print(warnmsg) 
            rate.sleep()
    except rospy.ROSInterruptException:
        pass


#!/usr/bin/env python

from __future__ import print_function

import sys
import rospy
from beginner_tutorials.srv import *

def servo_update_client(id, angle):
    rospy.wait_for_service('servo')
    try:
        servo_server = rospy.ServiceProxy('servo', ServoUpdate)
        resp1 = servo_server(id, angle)
        return resp1.result
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def usage():
    return "%s [id angle]"%sys.argv[0]

if __name__ == "__main__":
    if len(sys.argv) == 3:
        id = int(sys.argv[1])
        angle = float(sys.argv[2])
    else:
        print(usage())
        sys.exit(1)
    print("Setting servo %s to %s"%(id, angle))
    ret = servo_update_client(id, angle)
    if ret == 1:
        print("servo %s angle = %s"%(id, angle))
    else:
        print("servo %s set angle failed"%(id))
 
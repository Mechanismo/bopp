#!/usr/bin/env python

import rospy
import math
import numpy
from beginner_tutorials.GY85.sensor_reader import SensorReader

from std_msgs.msg import String

from sensor_msgs.msg import Imu

sensor_reader = SensorReader()

gyr_zero = [0.0,0.0,0.0]
acc_zero = [0.0,0.0,0.0]
quat = [0,0,0,0]
mrot = []

class SensorPublisher:
    def __init__(self,pub):
        self.pub = pub

    def on_sensor_data_changed(self, reading):
        global pub
        print(str(reading))
        # imu_str = "IMU:0,0,0,0,0,0,0,0,0,0,0,0,0,0,%s" % rospy.get_time()
        imu_str = str(reading)
        rospy.loginfo(imu_str)
        self.pub.publish(imu_str)
        return True

def imu_ready():
    global sensor_reader


def do_imu_loop():
    #global pub = rospy.Publisher('imu_node', String, queue_size=1)
    #rospy.init_node('imu')
    rate = rospy.Rate(1) # 10hz
    while not rospy.is_shutdown():
        # imu_str = "IMU:0,0,0,0,0,0,0,0,0,0,0,0,0,0,%s" % rospy.get_time()
        # rospy.loginfo(imu_str)
        # pub.publish(imu_str)
        rate.sleep()

def rotation_matrix_from_vectors(vec1, vec2):
    """ Find the rotation matrix that aligns vec1 to vec2
    :param vec1: A 3d "source" vector
    :param vec2: A 3d "destination" vector
    :return mat: A transform matrix (3x3) which when applied to vec1, aligns it with vec2.
    """
    a, b = (vec1 / numpy.linalg.norm(vec1)).reshape(3), (vec2 / numpy.linalg.norm(vec2)).reshape(3)
    v = numpy.cross(a, b)
    c = numpy.dot(a, b)
    s = numpy.linalg.norm(v)
    kmat = numpy.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    rotation_matrix = numpy.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s ** 2))
    return rotation_matrix

def quaternion_from_vectors(a, b):
    """ Find the rotation quaternion a to b
    :param a: A 3d "source" vector
    :param b: A 3d "destination" vector
    :return mat: A quaternion
    """
    
    v = numpy.cross(a, b)
    c = numpy.dot(a, b)
    # s = numpy.linalg.norm(v)
    #s = 1.0

    quat = [v[0],v[1],v[2],c]

    quatlen = numpy.linalg.norm(quat)
    print("quaternion len=", quatlen)
    return quat

def calibrate():
    global gyr_zero
    global acc_zero
    global mrot
    global quat
    rate = rospy.Rate(30)
    nsample = 0
    gsum = [0,0,0]
    asum = [0,0,0]
    print("calibrating imu...")
    while not rospy.is_shutdown() and nsample < 100:
        gyr = sensor_reader.gyroscope.read_data()
        acc = sensor_reader.accelerometer.read_data()
        nsample += 1
        gsum = numpy.add(gsum,gyr)
        asum = numpy.add(asum,acc)
        rate.sleep()

    print(gsum)

    gyr_zero = gsum / float(nsample)
    acc_zero = asum / float(nsample)
    
    # normalise 
    acc_mag = numpy.linalg.norm(acc_zero)
    norm_acc = acc_zero / acc_mag

    # theta = math.acos(acc_z_zero)
    vup = [0.0,0.0,1.0]
    
    quat = quaternion_from_vectors(vup,norm_acc)

    #mrot = rotation_matrix_from_vectors(norm_acc,vup)
    # mrot = mrot * -1.0
    mrot = rotation_matrix_from_vectors(vup,norm_acc)

    print("gyro zero : ",gyr_zero)
    print("acc  zero : ",acc_zero)
    print("quaternion :",quat)
    #print("vaz :",vaz)
    print(mrot)

if __name__ == '__main__':
    try:
        degs2_rads = math.pi / 180.0

        q_orient = [0.0,0.0,0.0,1.0]

        pub = rospy.Publisher('imu_node', Imu, queue_size=1)
        imuMsg = Imu()
        imuMsg.header.frame_id = "base_imu"
        #imuMsg.orientation_covariance = [
        #-1, 0 , 0,
        #0, 0, 0,
        #0, 0, 0]
        imuMsg.orientation_covariance = [
        0.02 , 0 , 0,
        0, 0.02, 0,
        0, 0, 0.02]
        imuMsg.angular_velocity_covariance = [
        0.02, 0 , 0,
        0 , 0.02, 0,
        0 , 0 , 0.02]
        imuMsg.linear_acceleration_covariance = [
        0.04 , 0 , 0,
        0 , 0.04, 0,
        0 , 0 , 0.04]

        #sred = SensorPublisher(pub)
        #sensor_reader.set_sensor_listener(sred)
        rospy.init_node('imu')

        sensor_reader.start_reading()
        rate = rospy.Rate(30) # 10hz

        calibrate()

        while not rospy.is_shutdown():
            # imu_str = "IMU:0,0,0,0,0,0,0,0,0,0,0,0,0,0,%s" % rospy.get_time()
            # rospy.loginfo(imu_str)
            try:
                imuMsg.header.stamp = rospy.Time.now()

                imuMsg.orientation.x = q_orient[0]
                imuMsg.orientation.y = q_orient[1]
                imuMsg.orientation.z = q_orient[2]
                imuMsg.orientation.w = q_orient[3]


                acc = sensor_reader.accelerometer.read_data()

                # rot_acc = norm_acc * mrot
                acc = numpy.dot(acc,mrot)

                if math.fabs(acc[0]) < 0.04:
                    acc = [0.0,acc[1],acc[2]]
                if math.fabs(acc[1]) < 0.04:
                    acc = [acc[0],0.0,acc[2]]
                if math.fabs(acc[2]) < 0.04:
                    acc = [acc[0],acc[1],0.0]

                acc_mag = numpy.linalg.norm(acc)
                norm_acc = acc / acc_mag

                #acc_fix = numpy.dot(acc,mrot)
                #print(acc)
                #print(mrot)
                #acc_fix = acc

                #imuMsg.linear_acceleration.x = acc[0] * 9.81
                #imuMsg.linear_acceleration.y = acc[1] * 9.81
                #imuMsg.linear_acceleration.z = acc[2] * 9.81
                
                imuMsg.linear_acceleration.x = norm_acc[0] * 9.81
                imuMsg.linear_acceleration.y = norm_acc[1] * 9.81
                imuMsg.linear_acceleration.z = norm_acc[2] * 9.81


                #imuMsg.linear_acceleration.x = rot_acc[0] * 9.81
                #imuMsg.linear_acceleration.y = rot_acc[1] * 9.81
                #imuMsg.linear_acceleration.z = rot_acc[2] * 9.81

                gyr = sensor_reader.gyroscope.read_data()
                imuMsg.angular_velocity.x = (gyr[0]-gyr_zero[0]) / 14.375 * degs2_rads
                imuMsg.angular_velocity.y = (gyr[1]-gyr_zero[1]) / 14.375 * degs2_rads
                imuMsg.angular_velocity.z = (gyr[2]-gyr_zero[2]) / 14.375 * degs2_rads
                pub.publish(imuMsg)
            except OSError:
                # Getting occasional I2C errors on Pi
                # timing errors, or noise?
                warnmsg = "OSError reading I2C IMU"
                rospy.logwarn(warnmsg)
                print(warnmsg) 
            #sensor_reader.read()
            rate.sleep()
        #do_imu_loop()
    except rospy.ROSInterruptException:
        pass


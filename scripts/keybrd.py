#!/usr/bin/python
#
#   useage:
#       rosrun beginner_tutorials keybrd.py
#
import time
import math
import requests
from requests.auth import HTTPBasicAuth
import atexit
import struct
import fcntl
from evdev import InputDevice, ecodes, categorize
from select import select

import rospy
from std_msgs.msg import String, Int16
from beginner_tutorials.srv import *
from geometry_msgs.msg import Twist, TwistStamped, Vector3
from beginner_tutorials import crawly_stuff

# Define pins for Pan/Tilt
pan = 0
tilt = 1
tVal = 0 # 0 degrees is centre
pVal = 0 # 0 degrees is centre
boppVal = 0

speed = 60

wagfreq = 0


#======================================================================
# Reading single character by forcing stdin to raw mode
import os
import sys
import tty
import termios


old_settings=None

def init_anykey():
   global old_settings
   old_settings = termios.tcgetattr(sys.stdin)
   new_settings = termios.tcgetattr(sys.stdin)
   new_settings[3] = new_settings[3] & ~(termios.ECHO | termios.ICANON) # lflags
   new_settings[6][termios.VMIN] = 0  # cc
   new_settings[6][termios.VTIME] = 0 # cc
   termios.tcsetattr(sys.stdin, termios.TCSADRAIN, new_settings)

@atexit.register
def term_anykey():
   global old_settings
   if old_settings:
      termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)

def anykey():
    #ch_set = []
    ch = os.read(sys.stdin.fileno(), 1)
    #while ch != None and len(ch) > 0:
    #   ch_set.append( ord(ch[0]) )
    #   ch = os.read(sys.stdin.fileno(), 1)
    #return ch_set
    if ch == '0x03':
        raise KeyboardInterrupt
    return ch

def readchar():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    if ch == '0x03':
        raise KeyboardInterrupt
    return ch

def readkey(getchar_fn=None):
    # getchar = getchar_fn or readchar
    getchar = getchar_fn or anykey
    c1 = getchar()
    if not c1: return None
    if ord(c1) != 0x1b:
        return c1.decode()
    c2 = getchar()
    if ord(c2) != 0x5b:
        return c1
    c3 = getchar().decode()
    return chr(0x10 + ord(c3) - 65)  # 16=Up, 17=Down, 18=Right, 19=Left arrows

init_anykey()

# End of single character reading
#======================================================================

def servo_update_client(id, angle):
    rospy.wait_for_service('servo')
    try:
        servo_server = rospy.ServiceProxy('servo', ServoUpdate)
        resp1 = servo_server(id, angle)
        return resp1.result
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def pub_cmdvel( linear_x, angular_z ):
    global pub_cmd_mode
    global _cls
    global _stamped
    global _frame_id
    global _pub_cmd_vel

    if pub_cmd_mode == 0 or pub_cmd_mode == 1 or pub_cmd_mode == 2:
        to_pub = _cls()
        if _stamped:
            to_pub.header.stamp = rospy.Time.now()
            to_pub.header.frame_id = _frame_id
            twist = to_pub.twist
        else:
            twist = to_pub

        twist.linear = Vector3(linear_x,0,0)
        twist.angular = Vector3(0,0,angular_z)
        _pub_cmd_vel.publish(to_pub)

def spinLeft(speed):
    #rospy.wait_for_service('spinleft')
    #try:
    #    ss = rospy.ServiceProxy('spinleft', SpeedUpdate)
    #    resp1 = ss(speed)
    #    return resp1.result
    #except rospy.ServiceException as e:
    #    print("Service call failed: %s"%e)
    pub_cmdvel(0,speed/100*0.8)

def spinRight(speed):
    #rospy.wait_for_service('spinright')
    #try:
    #    ss = rospy.ServiceProxy('spinright', SpeedUpdate)
    #    resp1 = ss(speed)
    #    return resp1.result
    #except rospy.ServiceException as e:
    #    print("Service call failed: %s"%e)
    pub_cmdvel(0,-speed/100*0.8)

def forward(speed):
    # OLD - call forward / service
    #rospy.wait_for_service('forward')
    #try:
    #    ss = rospy.ServiceProxy('forward', SpeedUpdate)
    #    resp1 = ss(speed)
    #    return resp1.result
    #except rospy.ServiceException as e:
    #    print("Service call failed: %s"%e)
    pub_cmdvel(speed/100*1.5,0)

def reverse(speed):
    #rospy.wait_for_service('reverse')
    #try:
    #    ss = rospy.ServiceProxy('reverse', SpeedUpdate)
    #    resp1 = ss(speed)
    #    return resp1.result
    #except rospy.ServiceException as e:
    #    print("Service call failed: %s"%e)
    pub_cmdvel(-speed/100*1.5,0)

def robot_stop():
    #rospy.wait_for_service('stop')
    #try:
    #    ss = rospy.ServiceProxy('stop', SpeedUpdate)
    #    resp1 = ss(0)
    #    return resp1.result
    #except rospy.ServiceException as e:
    #    print("Service call failed: %s"%e)
    pub_cmdvel(0,0)

def pixels_on(rr,gg,bb):
    rospy.wait_for_service('fill')
    try:
        ss = rospy.ServiceProxy('fill', PixelUpdate)
        resp1 = ss(0,rr,gg,bb)
        return resp1.result
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def pixels_off():
    rospy.wait_for_service('off')
    try:
        ss = rospy.ServiceProxy('off', PixelUpdate)
        resp1 = ss(0,0,0,0)
        return resp1.result
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def camera_record():
    # url = "http://localhost:8080/1/config/set?ffmpeg_output_movies=on"
    url = "http://localhost:8080/0/detection/start" 
    x = requests.get(url, auth=HTTPBasicAuth('andy', 'boppy3'))
    print(x)
    # myobj = {'somekey': 'somevalue'}
    #myobj = {}
    #x = requests.post(url, data = myobj)
    

def camera_stoprecord():
    #url = "http://localhost:8080/1/config/set?ffmpeg_output_movies=off"
    url = "http://localhost:8080/0/detection/pause"
    x = requests.get(url, auth=HTTPBasicAuth('andy', 'boppy3'))
    print(x)
    # myobj = {'somekey': 'somevalue'}
    #myobj = {}
    #x = requests.post(url, data = myobj)

def call_wag_service(freq):
    rospy.wait_for_service('wag')
    try:
        wag_server = rospy.ServiceProxy('wag', SpeedUpdate)
        resp1 = wag_server(freq)
        return resp1.result
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

pub_cmd_mode = 0

def _on_cmdvel_mode_out(data):
    global pub_cmd_mode
    #if pub_cmd_mode == data.data: return

    pub_cmd_mode = data.data
    if pub_cmd_mode == 0:
        print("driving Bopp!\r\n")
    elif pub_cmd_mode == 1:
        print("driving Krab!\r\n")
    elif pub_cmd_mode == 2:
        print("driving Crawly-Bot!\r\n")
        crawly_stuff.get_connected()
    else:
        print("drive disabled.\r\n")

def set_cmdmode(cmode):
    rospy.wait_for_service('cmdvel_mode')
    try:
        rospy.loginfo("set cmdvel_mode = {}".format(cmode))
        ss = rospy.ServiceProxy('cmdvel_mode', PixelUpdate)
        resp1 = ss(cmode,0,0,0)
        return resp1.result
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def next_cmdvel_mode():
    global pub_cmd_mode
    if pub_cmd_mode >= 3:
        pub_cmd_mode = 0
    else:
        pub_cmd_mode += 1
    set_cmdmode(pub_cmd_mode)

_stamped = False
_cls = None
_frame_id = None
_pub_cmd_vel = None

scancodes = {
    # Scancode: ASCIICode
    0: None, 1: u'ESC', 2: u'1', 3: u'2', 4: u'3', 5: u'4', 6: u'5', 7: u'6', 8: u'7', 9: u'8',
    10: u'9', 11: u'0', 12: u'-', 13: u'=', 14: u'BKSP', 15: u'TAB', 16: u'Q', 17: u'W', 18: u'E', 19: u'R',
    20: u'T', 21: u'Y', 22: u'U', 23: u'I', 24: u'O', 25: u'P', 26: u'[', 27: u']', 28: u'CRLF', 29: u'LCTRL',
    30: u'A', 31: u'S', 32: u'D', 33: u'F', 34: u'G', 35: u'H', 36: u'J', 37: u'K', 38: u'L', 39: u';',
    40: u'"', 41: u'`', 42: u'LSHFT', 43: u'\\', 44: u'Z', 45: u'X', 46: u'C', 47: u'V', 48: u'B', 49: u'N',
    50: u'M', 51: u',', 52: u'.', 53: u'/', 54: u'RSHFT', 56: u'LALT', 57: u' ', 100: u'RALT',
    ecodes.KEY_UP: chr(16), ecodes.KEY_DOWN: chr(17), ecodes.KEY_LEFT: chr(19), ecodes.KEY_RIGHT: chr(18)
}

def keybrd_server():
    global speed
    global pVal
    global boppVal
    global wagfreq
    global _stamped
    global _cls
    global _frame_id
    global _pub_cmd_vel
    wagns = 0

    is_recording = False

    pub = rospy.Publisher('keybrd', String, queue_size=10)
    rospy.Subscriber('cmdvel_mode_out', Int16, _on_cmdvel_mode_out, queue_size=10)
    # rate = rospy.Rate(10) # 10hz

    _stamped = rospy.get_param('~stamped', False)
    if _stamped:
        _cls = TwistStamped
        _frame_id = rospy.get_param('~frame_id', 'base_link')
    else:
        _cls = Twist    

    _pub_cmd_vel = rospy.Publisher('cmd_vel', _cls, queue_size=1)

    rospy.init_node('keybrd', anonymous=True)
    
    crawly_stuff.init()

    stop_timer = 0
    stop_time_start = 0

    #phys_keyboard = open("/dev/input/event0","rb")
    #fcntl.fcntl(phys_keyboard, fcntl.F_SETFL, 0);

    try:
        phys_keyboard = InputDevice('/dev/input/event0')
    except FileNotFoundError:
        phys_keyboard = None

    try:
        while not rospy.is_shutdown():

            tnow = time.time()
            if stop_timer > 0 and tnow >= stop_timer:
                print("STOP")
                stop_timer = 0
                robot_stop()

            key = None

            if phys_keyboard:
                # only do this for blocking reads
                # r,w,x = select([phys_keyboard], [], [])
                
                event = phys_keyboard.read_one()
                if event:
                    if event.type == ecodes.EV_KEY: # and event.value==1:
                        data = categorize(event)
                        print(data)
                        if (data.keystate == 1 or data.keystate == 2) and event.code in scancodes:
                            # print(ecodes.KEY[event.code])
                            print(scancodes[event.code])
                            key = scancodes[event.code]

            if key is None:
                key = readkey()
                if key is None: 
                    time.sleep(0.01)
                    continue

            pub.publish(str(key))
            #rate.sleep()

            if pub_cmd_mode == 2:
                # if driving crawly... 
                # print("TEST key:{} num_patterns:{}".format(key,len(crawly_stuff.KEY_PATTERN)))

                if is_recording:
                    is_recording = False
                    if key == chr(0x1b):
                        continue

                    pattern = []
                    for id in crawly_stuff.DXL_ID:
                        pos = crawly_stuff.read_position(id)
                        pattern.append([id,116,pos])
                    crawly_stuff.KEY_PATTERN[key] = pattern
                    print("pattern added")
                    print(pattern)
                    continue

                if crawly_stuff.key_pattern(key):
                    print("Crawly did it!")
                    continue
                if key == '.':
                    crawly_stuff.toggle_torque_enable()
                    if crawly_stuff.get_torque_state():
                        print("torque enabled")
                    else:
                        print("torque disabled")
                    continue
                elif key.upper() == 'R':
                    print("Set pose pattern, then press a key to assign it to")
                    is_recording = True
                    continue
                # note Rii event is US key layout (?)
                elif key == '\'' or key == '\"':   # note using Z for body moves
                    print("Zeroing servos...")
                    crawly_stuff.zero_dynamixels()
                    continue

                elif key == '?':
                    print("Servo positions...")
                    for id in crawly_stuff.DXL_ID:
                        pos = crawly_stuff.read_position(id)
                        if pos is None:
                            print("DXL[{0:d}]: ERR".format(id))    
                        else:
                            print("DXL[{0:d}]: {1:d}".format(id,pos))

            if key == ' ':
                pVal = 0
                servo_update_client(0, pVal)
                print("Centre", pVal)
            elif key.upper() == 'L':
                pVal = -90
                servo_update_client(0, pVal)
                print("Left", pVal)
            elif key.upper() == 'B':
                if wagfreq > 0:
                    wagfreq = 0
                    call_wag_service(wagfreq)
                if boppVal > 0:
                    boppVal = -30
                else:
                    boppVal = 20
                
                servo_update_client(1, boppVal)
                print("bopp ", boppVal)
            elif key.upper() == 'R':
                pVal = 90
                servo_update_client(0, pVal)
                print("Right", pVal)
            #elif key =='x':
            #    initio.stopServos()
            #    print("Stop")
            elif key == '[':
                camera_record()
                print("recording...smile!")
            elif key == ']':
                camera_stoprecord()
                print("recording stopped")
            elif key == '-':
                speed -= 10
                if speed < 0: speed = 0
                print("speed ", speed)
            elif key == '=' or key == '+':
                speed += 10
                if speed > 100: speed = 100
                print("speed ", speed)
            elif key == ',':
                pVal = max(-90, pVal-5)
                servo_update_client(0, pVal)
                print("pan left", pVal)
                #dist = robohat.getDistance()
                #print("Distance: ", int(dist))

            elif key == '.':
                pVal = min(90, pVal+5)
                print("pan right", pVal)
                servo_update_client(0, pVal)
                #dist = robohat.getDistance()
                #print("dist: ", int(dist))

            elif key.upper() == 'W' or ord(key) == 16:
                print("forward", speed)
                forward(speed)
                stop_timer = time.time() + 0.3
                #time.sleep(0.3)
                #robot_stop()
                #dist = robohat.getDistance()
                #print("dist: ", int(dist))

            elif key.upper() == 'A' or ord(key) == 19:
                print('Spin Left', speed)
                spinLeft(speed)
                stop_timer = time.time() + 0.2
                #time.sleep(0.2)
                #robot_stop()
                #dist = robohat.getDistance()
                #print("dist: ", int(dist))

            elif key.upper() == 'S' or ord(key) == 18:
                print('Spin Right', speed)
                spinRight(speed)
                stop_timer = time.time() + 0.2
                #time.sleep(0.2)
                #robot_stop()
                #dist = robohat.getDistance()
                #print("dist: ", int(dist))

            elif key.upper() == 'Z' or ord(key) == 17:
                print( "reverse", speed)
                reverse(speed)
                stop_timer = time.time() + 0.2
                #time.sleep(0.2)
                #robot_stop()
                #dist = robohat.getDistance()
                #print("dist:b", int(dist))

            elif key == '1':
                pixels_on(255,255,255)
                print("white")
            elif key == '2':
                pixels_on(255,0,0)
                print("red")
            elif key == '3':
                pixels_on(0,255,0)
                print("green")
            elif key == '4':
                pixels_on(0,0,255)
                print("blue")
            elif key == '5':
                pixels_on(255,0,255)
                print("purple")
            elif key == '6':
                pixels_on(255,255,0)
                print("yellow")
            elif key == '7':
                pixels_on(0,255,255)
                print("cyan")
            elif key == '0':
                pixels_off()
                print("off")
            elif key.upper() == 'Q':
                """
                tmns = time.time_ns()
                if wagns > 0:
                    if tmns - wagns > 4e9:
                        wagfreq = 0
                    else:
                        wagfreq = 1e9 / float(tmns - wagns)
                wagns = tmns
                """
                if wagfreq >= 3:
                    wagfreq = 0
                else:
                    wagfreq += 0.25
                print("wag freq",wagfreq)
                call_wag_service(wagfreq)
            elif key.upper() == 'M':
                # mode
                next_cmdvel_mode()

            elif ord(key) == 3:
                break
            else:
                print("key",ord(key),key)
 
    except KeyboardInterrupt:
        print

    #finally:
    #    robohat.cleanup()

if __name__ == "__main__":
    keybrd_server()


#!/usr/bin/env python

import time
import board
import neopixel
import math

import rospy
from beginner_tutorials.srv import PixelUpdate,PixelUpdateResponse
from std_msgs.msg import String

pixel_pin = board.D18
num_pixels = 8
ORDER = neopixel.GRB

pxstate = True
pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER)

# 0 - off
# 1 - on
pmode = 0

def update_pixel(data):
    global pxstate
    global pixels
    #rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)
    #if data.data == 'OFF':
    #    pmode = 0
    #else:
    #    pmode = 1
    
    #if pmode == 0:
    #    pixels.fill((0,0,0))
    #else:
    #    if pxstate:
    #        pixels.fill((0,255,0))
    #    else:
    #        pixels.fill((255,0,0))
    #    pxstate = not pxstate
    pixels[data.id] = (data.r,data.g,data.b)
    pixels.show()
    return PixelUpdateResponse(1)

def fill_pixels(data):
    global pixels
    pixels.fill((data.r,data.g,data.b))
    pixels.show()
    return PixelUpdateResponse(1)

def off_pixels(data):
    global pixels
    pixels.fill((0,0,0))
    pixels.show()
    return PixelUpdateResponse(1)

def srv_pixel():
    rospy.init_node('neopixel', anonymous=True)
    s = rospy.Service('pixel', PixelUpdate, update_pixel)
    s = rospy.Service('fill', PixelUpdate, fill_pixels)
    s = rospy.Service('off', PixelUpdate, off_pixels)
    # rospy.Subscriber('chatter', String, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    srv_pixel()
